# Hairdresser Website
---

A responsive single page Hairdresser website.

This project was created as a part of a local Web Development course I took. We had to use the skills we've learnt by  
then and build this website based on the wireframe they gave us.


## Built With
---

* HTML5
* CSS3
* Bootstrap
* [Pixabay](https://pixabay.com/) - Photos  

